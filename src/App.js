import { useState, Fragment } from 'react';
import { Container } from 'react-bootstrap';
import './App.css'

function App(){
  const [input1, setInput1] = useState('');
  const [input2, setInput2] = useState('');
  const [total, setTotal] = useState(0);


function addNumber(){setTotal(`${input1 + input2}`)}
function subtractNumber(){ setTotal(`${input1 - input2}`)}
function multiplyNumber(){setTotal(`${input1 * input2}`)}
function divideNumber(){setTotal(input1 / input2)}
function resetNumber(){
  setInput1('');
  setInput2('');
  setTotal(0);
}

  return (
    <Fragment  >
      <Container className='App mx-auto'  >
        <div >
          <h1>Calculator</h1>
          <h2>{total}</h2>
        </div>
    <Container className="container">
          <div >
            <input className="numberInputs"
              type="number"
              value={input1}
              onChange={e => setInput1(e.target.value)}
              placeholder="0"
            />
            <input className="numberInputs" 
              type="number"
              value={input2}
              onChange={e => setInput2(e.target.value)}
              placeholder="0"
            />
          </div>
          <br/>
      </Container>
      <Container className='oprationBtn' >
        <button className='mx-4' onClick={addNumber}>Add</button>
        <button className='mx-4' onClick={subtractNumber}>Subtract</button>
        <button className='mx-4' onClick={multiplyNumber}>Multiply</button>
        <button className='mx-4' onClick={divideNumber}>Divide</button>
        <button className='mx-4' onClick={resetNumber}>Reset</button>
      </Container>
    </Container>
    </Fragment>
  );

}

export default App;
